#！usr/bin/perl 

use strict;
use warnings;
use IO::Handle;
use constant LOGFILE=>'output.log';
use constant CMDFILE=>'cmd.txt';
#use feature qw(say, switch);
use 5.010;

#package list
our @pack_list;
#store return value status
our $return_status;

#execution: install a list of packages
sub execution{
  my $cmdname = shift @_;
  my $filename = LOGFILE; #JZ: Make the filename defined in the beginning, not hard-coded 
  #open and write stderr to logfile
  open(FH, ">$filename") or die "the file cannot be opened: $!\n";
  #redirect stderr to logfile
  STDERR->fdopen(\*FH, 'w') or die $!;
  #use backticks operator to execute external cmd
  my $res = `$cmdname`;
  #store status value to global variable
  $return_status = $?;
  #write stderr output to logfile
  print FH $res;
  #close filehandler
  close(FH);
} 

#post-checking: check whether it is successfully installed
sub post_checking{
  my $cmdname = shift @_;
  open(RD, LOGFILE); #JZ: Why?
  
  #if return value is 0, execute successfully 
  if($return_status == 0){ #JZ: Is this the return value of the previous open command?
    print "Successfully execute this command $cmdname\n";
    return ;
  }
  while(<RD>){
    given($_){
      when (/You need to be root to perform this command./i){
        close(RD);
        say "You need to be root to execute $cmdname!";
        exit;
      }
      when (/Nothing to do/i){
        say "Package $cmdname is already installed.";
        #terminate the loop statement 
        last;
      }
      when (/mkdir:.*File exists/is){
        say 'File exists, do you want continue or not, please input YES, NO';
        &stdinput();
      }
      default {
        say "None of the above errors";
      }
    }
  }  
  close(RD); 
}

sub stdinput{
  while(<>){
    chomp;
    if($_ eq "YES"){
      last;
    }elsif($_ eq "NO"){
      die "execution end!\n";
    }else{
      print "File exists, do you want continue or not, please input YES, NO \n";
    }
  }
}

open(CMD, CMDFILE); #JZ: refer to the comments on output.log
while(<CMD>){
  chomp($_);
  push (@pack_list, $_);
}
close(CMD);

#execute each cmds
foreach my $current(@pack_list){
    print "current execution cmd: ", $current, "\n";
    &execution($current);
    &post_checking($current);
}
